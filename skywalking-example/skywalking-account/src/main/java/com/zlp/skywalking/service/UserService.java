package com.zlp.skywalking.service;

import com.zlp.skywalking.dto.OrderDTO;
import com.zlp.skywalking.dto.UserDTO;

public interface UserService {

    OrderDTO saveOrder(UserDTO orderDTO);

    UserDTO getUserOrder(Long userId);
}
