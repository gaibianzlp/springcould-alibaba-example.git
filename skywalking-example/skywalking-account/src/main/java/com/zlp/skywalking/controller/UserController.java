package com.zlp.skywalking.controller;

import com.zlp.skywalking.dto.OrderDTO;
import com.zlp.skywalking.dto.UserDTO;
import com.zlp.skywalking.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;


    @GetMapping("/saveOrder")
    public OrderDTO saveOrder(){

        OrderDTO orderDTO = userService.saveOrder(new UserDTO());
        return orderDTO;
    }


    /** 
     * 获取用户订单
     * @date: 2021/11/9 14:25
     */
    @GetMapping("/getUserOrder")
    public UserDTO getUserOrder(@RequestParam("userId") Long userId){

        return userService.getUserOrder(userId);
    }
}
