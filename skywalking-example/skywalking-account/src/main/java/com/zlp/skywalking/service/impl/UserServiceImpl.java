package com.zlp.skywalking.service.impl;

import com.zlp.skywalking.dto.OrderDTO;
import com.zlp.skywalking.dto.UserDTO;
import com.zlp.skywalking.service.OrderService;
import com.zlp.skywalking.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.skywalking.apm.toolkit.trace.Tag;
import org.apache.skywalking.apm.toolkit.trace.Tags;
import org.apache.skywalking.apm.toolkit.trace.Trace;
import org.apache.skywalking.apm.toolkit.trace.TraceContext;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
@RequiredArgsConstructor
@Slf4j(topic = "UserServiceImpl")
public class UserServiceImpl implements UserService {


    private final OrderService orderService;


    @Override
    public OrderDTO saveOrder(UserDTO userDTO) {

        log.info("添加订单");
        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setId(1L);
        orderDTO.setOrderNo(System.currentTimeMillis() + "");
        orderDTO.setName("iphone 13pro");
        orderDTO.setPrice(100L);
        orderService.addOrder(orderDTO);
        return orderDTO;
    }


    @SneakyThrows
    @Trace
    @Tags({
        @Tag(key = "getUserOrder", value = "returnedObj"),
        @Tag(key = "param", value = "arg[0]")
    })
    @Override
    public UserDTO getUserOrder(Long userId) {

        String traceId = TraceContext.traceId();

        UserDTO userDTO = new UserDTO();
        userDTO.setUserId(userId);
        userDTO.setUserName("给自己一个smile");
        userDTO.setAddress("上海市浦东新区张江镇");
        userDTO.setTraceId(traceId);
        OrderDTO orderDTO = orderService.getOrderByUserId(userId);
        TimeUnit.SECONDS.sleep(2L);
        userDTO.setOrderDTO(orderDTO);
        return userDTO;
    }
}
