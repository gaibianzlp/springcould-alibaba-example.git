package com.zlp.skywalking.service.impl;

import com.alibaba.fastjson.JSON;
import com.zlp.skywalking.dto.OrderDTO;
import com.zlp.skywalking.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@Service
@RestController
@Slf4j(topic = "OrderServiceImpl")
public class OrderServiceImpl implements OrderService {


    @Override
    @PostMapping("/addOrder")
    public OrderDTO addOrder(OrderDTO orderDTO) {

        log.info("添加订单 orderDTO={}", JSON.toJSONString(orderDTO));
        orderDTO.setId(9999L);
        return orderDTO;
    }

    @Override
    public OrderDTO getOrderByUserId(Long userId) {

        log.info("getOrderById.req userId={}",userId);
        OrderDTO orderDTO = new OrderDTO();
        Long index = 1L / userId;
        orderDTO.setId(99L);
        orderDTO.setUserId(userId);
        orderDTO.setOrderNo(System.currentTimeMillis()+"");
        orderDTO.setName("iphone 13pro"+System.currentTimeMillis());
        orderDTO.setPrice(100L);
        return orderDTO;
    }
}
