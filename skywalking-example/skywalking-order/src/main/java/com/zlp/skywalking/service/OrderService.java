package com.zlp.skywalking.service;

import com.zlp.skywalking.dto.OrderDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(contextId = "OrderService",name = "skywalking-order")
public interface OrderService {

    @PostMapping("addOrder")
    OrderDTO addOrder(OrderDTO orderDTO);

    @GetMapping("getOrderByUserId")
    OrderDTO getOrderByUserId(@RequestParam("userId") Long userId);
}
