package com.zlp.feign.stock.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("/stock")
public class StockController {

    @Value("${server.prot:8080}")
    private String port;

    @GetMapping("/reductStock")
    public String reductStock() {

        return "Hello study Feign port:" + port;
    }
}
