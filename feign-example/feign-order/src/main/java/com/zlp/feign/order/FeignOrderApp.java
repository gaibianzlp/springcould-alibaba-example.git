package com.zlp.feign.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@SpringBootApplication
public class FeignOrderApp {

    public static void main(String[] args) {
        SpringApplication.run(FeignOrderApp.class,args);
    }
}
