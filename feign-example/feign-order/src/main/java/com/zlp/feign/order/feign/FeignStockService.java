package com.zlp.feign.order.feign;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * FeignStockService
 * @date: 2021/12/6 20:32
 */
@FeignClient(value="feign-stock",contextId = "FeignStockService",path = "stock")
public interface FeignStockService {


    @GetMapping("/reductStock")
    String reductStock();

}
