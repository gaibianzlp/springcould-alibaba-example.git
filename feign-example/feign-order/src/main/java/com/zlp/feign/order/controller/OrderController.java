package com.zlp.feign.order.controller;

import com.zlp.feign.order.feign.FeignStockService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("order")
@RequiredArgsConstructor
public class OrderController {


    private final FeignStockService feignStockService;

    @GetMapping("addOrder")
    public String addOrder(){

        String stock = feignStockService.reductStock();
        return "order remoe call feign stock=>"+stock;
    }
}
