package com.zlp.sentinel;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class SentinelStockApp {


    public static void main(String[] args) {
        SpringApplication.run(SentinelStockApp.class,args);
    }


}
