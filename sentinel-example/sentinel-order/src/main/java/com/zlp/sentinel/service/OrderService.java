package com.zlp.sentinel.service;

public interface OrderService {

    String sayHello(String name);

    String addOrder(int orderId) ;

    String getUser();
}
