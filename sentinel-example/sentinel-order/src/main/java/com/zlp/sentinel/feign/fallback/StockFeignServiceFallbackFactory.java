package com.zlp.sentinel.feign.fallback;

import com.zlp.sentine.stock.feign.StockFeignService;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class StockFeignServiceFallbackFactory implements FallbackFactory<StockFeignService> {


    @Override
    public StockFeignService create(Throwable throwable) {

        return new StockFeignService() {

            @Override
            public String reduct(int orderId) {
                log.error("原因:{}", throwable.getMessage());
                return " 降级啦！！！";
            }
        };
    }
}
