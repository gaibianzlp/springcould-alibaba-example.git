package com.zlp.sentinel.common;

/**
 * 枚举了一些常用 API 操作码
 * @author ZouLiPing
 */
public enum RespCode implements IErrorCode{

    //系统
    SUCCESS(200, "操作成功"),
    FAILED(500, "操作失败"),
    VALIDATE_FAILED(404, "参数检验失败"),
    UNAUTHORIZED(401, "暂未登录或session已经过期"),
    FORBIDDEN(403, "没有相关权限"),
    WRONGUSERNAME(403, "用户名或密码错误"),
    UNKONW(999, "未知异常"),


    FLOW_EXCEPTION(100, "接口限流了"),
    DEGRADE_EXCEPTION(101, "服务降级了"),
    PARAM_FLOW_EXCEPTION(102, "热点参数限流了"),
    SYSTEM_BLOCK_EXCEPTION(103, "触发系统保护规则了"),
    AUTHORITY_EXCEPTION(104, "授权规则不通过"),



    ;






    private long code;
    private String message;

    RespCode(long code, String message) {
        this.code = code;
        this.message = message;
    }

    public long getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
