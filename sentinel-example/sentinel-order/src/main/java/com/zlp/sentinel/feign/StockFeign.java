package com.zlp.sentinel.feign;

import com.zlp.sentine.stock.feign.StockFeignService;
import com.zlp.sentinel.feign.fallback.StockFeignServiceFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;


@FeignClient(value="sentinel-stock",
        path = "/stock",
        contextId = "StockFeign",
        fallbackFactory = StockFeignServiceFallbackFactory.class
)
public interface StockFeign extends StockFeignService {


}
