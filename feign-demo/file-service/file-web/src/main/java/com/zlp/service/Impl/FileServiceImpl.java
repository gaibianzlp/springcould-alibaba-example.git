package com.zlp.service.Impl;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.zlp.config.oss.OssTemplate;
import com.zlp.dto.UploadResp;
import com.zlp.dto.UserDTO;
import com.zlp.service.FileService;
import com.zlp.utils.ConstantsUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.*;
import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
@Slf4j(topic = "FileServiceImpl")
public class FileServiceImpl implements FileService {

    private final OssTemplate ossTemplate;
    private final static String bucketName = "zlp-shop";

    @Override
    public UploadResp unionUpload(List<String> pathUrls) throws FileNotFoundException {

        log.info("uploadFile.req pathUrls={}", JSON.toJSONString(pathUrls));
        UploadResp uploadResp = new UploadResp();
        String originalFilename = "vidoe.mp4";
        uploadResp.setFileName(originalFilename);
        String filePath = IdUtil.simpleUUID() + StrUtil.DOT + FileUtil.extName(originalFilename);
        String localPath = "D:\\file\\tempfile\\video\\25601600mini.mp4";
        File file = new File(localPath);
        InputStream input = new FileInputStream(file);
        try {
            ossTemplate.putObject(bucketName, filePath, input);
            String fileUrl = ConstantsUtil.HTTP_PREFIX + bucketName + StrUtil.DOT + ConstantsUtil.DOMAIN + StrUtil.SLASH + filePath;
            uploadResp.setFileUrl(fileUrl);
            return uploadResp;
        } catch (Exception e) {
            log.error("上传失败", e);
        }
        return null;
    }

    @Override
    public UploadResp jsonUpload() {

        UploadResp uploadResp = new UploadResp();
        UserDTO userDTO = new UserDTO();
        userDTO.setUsername("ZouLiPing");
        userDTO.setPassword("123456");
        userDTO.setAddress("上海市浦东新区");
        userDTO.setAge(20);
        log.info("jsonUpload.req userDTO={}", userDTO);
        String jsonString = JSON.toJSONString(userDTO);
        ByteArrayInputStream inputStream = null;

        String originalFilename = "jsonUpload.json";
        uploadResp.setFileName(originalFilename);
        String filePath = "json" + StrUtil.SLASH + IdUtil.simpleUUID() + StrUtil.DOT + FileUtil.extName(originalFilename);
        try {
            inputStream = new ByteArrayInputStream(jsonString.trim().getBytes("utf-8"));
            ossTemplate.putObject(bucketName, filePath, inputStream);
            String fileUrl = ConstantsUtil.HTTP_PREFIX + bucketName + StrUtil.DOT + ConstantsUtil.DOMAIN + StrUtil.SLASH + filePath;
            uploadResp.setFileUrl(fileUrl);
            return uploadResp;
        } catch (Exception e) {
            log.error("上传失败", e);
        } finally {
            try {
                if (Objects.nonNull(inputStream)) {
                    inputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return uploadResp;
    }
}
