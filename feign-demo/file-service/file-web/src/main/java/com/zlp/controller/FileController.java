package com.zlp.controller;


import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.IdUtil;
import com.zlp.dto.UploadResp;
import com.zlp.service.FileService;
import com.zlp.utils.MultipartFileToFile;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.URI;
import java.util.List;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("file")
@Api(value = "文件模块")
public class FileController {


    private FileService fileService;


    @GetMapping("getUser")
    public String getUser(){
        return "success";
    }

    @GetMapping("unionUpload")
    @ApiOperation(value = "获取用户根据用户ID")
    public UploadResp unionUpload(List<String> pathUrls) throws FileNotFoundException {

        return fileService.unionUpload(pathUrls);
    }

    @GetMapping("jsonUpload")
    @ApiOperation(value = "上传JSON文件")
    public UploadResp jsonUpload() {

        return fileService.jsonUpload();
    }

    /**
     * 文件上传
     */
    @PostMapping("/uploadFile")
    @ApiOperation(value = "文件上传")
    public String uploadFile(@RequestParam("file") MultipartFile multipartFile) throws Exception {


        String targetDirectory = System.getProperty("user.dir") + File.separator + "logs";
        // 上传文件
        File file = MultipartFileToFile.multipartFileToFile(multipartFile);
        FileUtil.copy(file,new File(targetDirectory),Boolean.TRUE);
        String originalFilename = multipartFile.getOriginalFilename();
        log.info("originalFilename={}",originalFilename);
        String url = targetDirectory + File.separator + originalFilename;
        return url;
    }



}
