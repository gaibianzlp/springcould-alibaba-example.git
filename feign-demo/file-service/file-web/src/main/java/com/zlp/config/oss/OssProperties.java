package com.zlp.config.oss;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "oss")
public class OssProperties 
{
	private String endpoint;
	
	private String accessKeyId;
	
	private String accessKeySecret;
	/**
	 * 默认的存储桶名称
	 */
	private String bucketName;
}
