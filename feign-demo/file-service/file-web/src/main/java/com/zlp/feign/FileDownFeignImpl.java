package com.zlp.feign;

import com.aliyun.oss.model.OSSObject;
import com.zlp.config.oss.OssTemplate;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.*;

@RestController
@Slf4j(topic = "FileDownFeignImpl")
@Api(value = "FileDownFeign", tags = "文件下载模块")
public class FileDownFeignImpl implements FileDownFeign {


    @Resource
    private OssTemplate ossTemplate;

    @Override
    @PostMapping(value = "/v1/getObject")
    public ResponseEntity<byte[]> getObject(String downFileUrl) {

        downFileUrl = "test/edcb724a202d4497984cc48a240408d3.txt";
        OSSObject ossObject = ossTemplate.getObject(downFileUrl);
        HttpHeaders headers = new HttpHeaders();
        ResponseEntity<byte[]> entity = null;
        BufferedInputStream bis;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] buf = new byte[10240];
        try {
            bis = new BufferedInputStream(ossObject.getObjectContent());
            while (true) {
                int len = bis.read(buf);
                if (len < 0) {
                    break;
                }
                bos.write(buf, 0, len);
            }
            //将输出流转为字节数组，通过ResponseEntity<byte[]>返回
            byte[] b = bos.toByteArray();
            log.info("接收到的文件大小为：" + b.length);
            HttpStatus status = HttpStatus.OK;
            String imageName = "0001.txt";
            headers.setContentType(MediaType.ALL.APPLICATION_OCTET_STREAM);
            headers.add("Content-Disposition", "attachment;filename=" + imageName);
            entity = new ResponseEntity<>(b, headers, status);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (bos != null) {
                try {
                    bos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return entity;
    }

    /**
     * 文件（二进制数据）下载
     *
     * @param fileType 文件类型
     * @return
     */
    @GetMapping("/downloadFile")
    public ResponseEntity<byte[]> downloadFile(String fileType) {

        log.info("参数fileType: " + fileType);
        HttpHeaders headers = new HttpHeaders();
        ResponseEntity<byte[]> entity = null;
        InputStream in = null;
        try {
            in = new FileInputStream(new File("C:\\Users\\user.DESKTOP-8A9L631\\Desktop\\遂人\\图片\\0001.png"));
            int available = in.available();
            log.info("文件大小={}", available);
            byte[] bytes = new byte[available];
            String imageName = "001.png";
            imageName = new String(imageName.getBytes(), "iso-8859-1");
            in.read(bytes);
            headers.setContentType(MediaType.ALL.APPLICATION_OCTET_STREAM);
            headers.add("Content-Disposition", "attachment;filename=" + imageName);
            entity = new ResponseEntity<>(bytes, headers, HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return entity;
    }

}
