package com.zlp.config.oss;

import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.OSSObject;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;

import java.io.InputStream;
import java.util.Date;


@AllArgsConstructor
public class OssTemplate 
{
	private OSSClient client;
	//目前所有文件都存放在该命名空间下
	private final static String bucketName = "zlp-shop";
	
	@SneakyThrows
	public void putObject(String bucketName, String objectName, InputStream input) {
		client.putObject(bucketName, objectName, input);
	}

	@SneakyThrows
	public String generatePresignedUrl(String bucketName, String key) {
		
		Date expiration = new Date(new Date().getTime() + 3600l * 1000 * 24 * 365 * 10);	
		return client.generatePresignedUrl(bucketName, key, expiration).toString();
	}
	
	@SneakyThrows
	public void createBucket(String bucketName) {
		
		client.createBucket(bucketName);
	}

	@SneakyThrows
	public void deleteBucket(String bucketName) {
		
		client.deleteBucket(bucketName);
	}

	/**
	 * 删除OSS 文件
	 * @param delFileUrl  删除指定文件URL
	 */
	@SneakyThrows
	public void delFile(String delFileUrl){
		// key是文件名
		client.deleteObject(bucketName, delFileUrl);
//		client.shutdown();//关闭连接
	}


	public OSSObject getObject(String downFileUrl) {

		OSSObject object = client.getObject(bucketName, downFileUrl);
		return object;
	}
}
