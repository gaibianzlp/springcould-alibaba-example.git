package com.zlp.config.oss;

import com.aliyun.oss.OSSClient;
import lombok.AllArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@AllArgsConstructor
@EnableConfigurationProperties(OssProperties.class)
public class OssConfiguration 
{
	private OssProperties ossProperties;
	
	@Bean
	public OSSClient oSSClient() {
		@SuppressWarnings("deprecation")
		OSSClient client = new OSSClient(
				ossProperties.getEndpoint(), 
				ossProperties.getAccessKeyId(), 
				ossProperties.getAccessKeySecret());
		return client;
	}

	@Bean
	@ConditionalOnMissingBean(OssTemplate.class)
	@ConditionalOnBean(OSSClient.class)
	public OssTemplate ossTemplate(OSSClient client) {
		return new OssTemplate(client);
	}
}
