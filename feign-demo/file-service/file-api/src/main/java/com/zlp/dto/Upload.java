package com.zlp.dto;

import com.alibaba.fastjson.JSON;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
@Data
public class Upload {

    List<UploadResp> uploadResps;

    public static void main(String[] args) {
        Upload upload = new Upload();
        List<UploadResp> uploadResps = new ArrayList<>();
        UploadResp uploadResp = new UploadResp();
        uploadResp.setFileName("测试");
        uploadResp.setFileUrl("www.baidu.com");
        uploadResp.setVideoTime(45L);
        uploadResps.add(uploadResp);
        upload.setUploadResps(uploadResps);
        System.out.println(JSON.toJSONString(upload));

    }
}
