package com.zlp.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
@FeignClient(
        contextId = "fileDownFeign",
        name = "file-web"

)
public interface FileDownFeign {



    @PostMapping(value = "/v1/getObject",consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity<byte[]> getObject(String downFileUrl);

    @GetMapping("/downloadFile")
    ResponseEntity<byte[]> downloadFile(@RequestParam(value = "fileType")  String fileType) ;

}


