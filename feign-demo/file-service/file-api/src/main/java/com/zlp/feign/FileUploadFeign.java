package com.zlp.feign;
import com.zlp.config.FeignMultipartSupportConfig;
import com.zlp.dto.UploadResp;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@FeignClient(
        contextId = "fileUploadFeign",
        name = "file-web",
        configuration = FeignMultipartSupportConfig.class

)
public interface FileUploadFeign {

    /**
     * 单个文件上传
     * @RequestPart：文件传输
     * @param file 文件
     * @param type 目录
     * @date: 2021/3/26 9:27
     * @return: com.zlp.dto.UploadResp
     */
    @PostMapping(value = "/uploadFile", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    UploadResp uploadFile(@RequestPart(value = "file") MultipartFile file,
                          @RequestParam(value = "type") String type);


    /**
     * 多个文件上传
     * @RequestPart：文件传输
     * @param files 文件
     * @param type 目录
     * @date: 2021/3/26 9:27
     * @return: com.zlp.dto.UploadResp
     */
    @PostMapping(value = "/uploadFiles", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    List<UploadResp> uploadFiles(@RequestPart(value = "files") MultipartFile[] files, @RequestParam(value = "type") String type);

}


