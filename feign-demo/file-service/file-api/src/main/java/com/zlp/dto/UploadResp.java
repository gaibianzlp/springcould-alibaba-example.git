package com.zlp.dto;

import lombok.Data;

import java.util.List;


@Data
public class UploadResp {

    /**
     * 上传的文件名
     */
    private String fileName;

    /**
     * 图片访问地址
     */
    private String fileUrl;

    /**
     * 图片访问地址
     */
    private Long videoTime;

    public UploadResp(){}

    public UploadResp(String fileName, String fileUrl, Long videoTime){
        this.fileName = fileName;
        this.fileUrl = fileUrl;
        this.videoTime = videoTime;
    }



}
