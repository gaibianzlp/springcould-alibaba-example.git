package com.zlp.controller;

import com.zlp.dto.UploadResp;
import com.zlp.service.OrderService;
import com.zlp.utils.FileUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.List;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("order")
public class OrderController {

    private final OrderService orderService;
    private final FileUtil fileUtil;

    /**
     * 上传功能
     *
     * @param file MultipartFile
     * @date: 2021/4/14 10:21
     * @return: UploadResp
     */
    @PostMapping("uploadFile")
    public UploadResp uploadFile(@RequestParam("file") MultipartFile file) {

        return orderService.uploadFile(file);
    }


    /**
     * 下载功能
     * @param response
     * @param fileUrl
     * @date: 2021/4/14 10:21
     * @return: void
     */
    @GetMapping("getObject")
    public void getObject(HttpServletResponse response, String fileUrl) {

        orderService.getObject(response, fileUrl);
    }

    @GetMapping(value = "/sendUploadVoice")
    public void sendUploadVoice(HttpServletResponse response) {

        try {
            //获取文件
            String fileName = "test.xlsx";
            String localFileDir = "D:\\file\\video";
            fileName = URLDecoder.decode(fileName, "UTF-8");
            //获取文件输入流  localFileDir是服务端存储文件的路径
            File file = new File(localFileDir + File.separator + fileName);

            response.setContentLength((int) file.length());
            response.setCharacterEncoding("utf-8");
            response.setContentType("application/octet-stream");
            response.setHeader("Content-disposition", "attachment; filename=" + new String(fileName.getBytes("utf-8"), "ISO8859-1"));
            InputStream in = new BufferedInputStream(new FileInputStream(file), 4096);
            OutputStream os = new BufferedOutputStream(response.getOutputStream());
            byte[] bytes = new byte[4096];
            int i;
            while ((i = in.read(bytes)) > 0) {
                os.write(bytes, 0, i);
            }
            os.flush();
            os.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 下载数据包
     *
     * @param response
     * @date: 2021/3/25 15:36
     * @return: void
     */
    @GetMapping("downloadDataZip")
    public void downloadDataZip(HttpServletResponse response) {

        String title = "报告源数据包.zip";
        File filePath = new File(FileUtil.getTemplatePath() + File.separator + title);
        List<String> srcFiles = Arrays.asList("test/edcb724a202d4497984cc48a240408d3.txt");
        fileUtil.zipFeignFiles(srcFiles, filePath);
        String filename = System.currentTimeMillis()+"_"+title;
        //设置文件路径
        if (filePath.exists()) {
            FileInputStream fis = null;
            BufferedInputStream bis = null;
            try {
                response.setContentType("application/octet-stream");
                response.setHeader("Content-disposition", "attachment; filename=" + new String(filename.getBytes("utf-8"), "ISO8859-1"));
                byte[] buffer = new byte[4096];
                fis = new FileInputStream(filePath);
                bis = new BufferedInputStream(fis);
                OutputStream os = response.getOutputStream();
                int i = bis.read(buffer);
                while (i != -1) {
                    os.write(buffer, 0, i);
                    i = bis.read(buffer);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (bis != null) {
                    try {
                        bis.close();
                        // 删除临时文件
                        filePath.delete();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (fis != null) {
                    try {
                        fis.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }



   /* *//**
     * 下载数据包
     *
     * @param response
     * @date: 2021/3/25 15:36
     * @return: void
     */
    /*@GetMapping("downloadDataZip")
    public void downloadDataZip(HttpServletResponse response) {

        String title = "报告源数据包.zip";
        File filePath = new File(FileUtil.getTemplatePath() + File.separator + title);
        List<String> srcFiles = Arrays.asList("1.jpg", "2.jpg", "3.jpg","4.txt");
        FileUtil.zipFiles(srcFiles, filePath);
        String filename = System.currentTimeMillis() + "_" + title;
        //设置文件路径
        if (filePath.exists()) {
            FileInputStream fis = null;
            BufferedInputStream bis = null;
            try {
                response.setContentType("application/octet-stream");
                response.setHeader("Content-disposition", "attachment; filename=" + new String(filename.getBytes("utf-8"), "ISO8859-1"));
                byte[] buffer = new byte[4096];
                fis = new FileInputStream(filePath);
                bis = new BufferedInputStream(fis);
                OutputStream os = response.getOutputStream();
                int i = bis.read(buffer);
                while (i != -1) {
                    os.write(buffer, 0, i);
                    i = bis.read(buffer);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    // 删除临时文件
                    filePath.delete();
                    if (bis != null) {
                        bis.close();
                    }
                    if (fis != null) {
                        fis.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }*/

}
