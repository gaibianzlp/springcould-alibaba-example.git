package com.zlp.config.nacos;

import lombok.Data;

/**
 * nacos 配置 信息
 */
@Data
public class NacosConfigInfo {

    private String serverAddr;
    private String namespace;
    private String group;
    private String dataId;
    private boolean refresh = true;
    private Class cls = String.class;

    public NacosConfigInfo() {
    }
    public NacosConfigInfo(String serverAddr, String namespace, String group, String dataId, boolean refresh, Class cls) {
        this.serverAddr = serverAddr;
        this.namespace = namespace;
        this.group = group;
        this.dataId = dataId;
        this.refresh = refresh;
        this.cls = cls;
    }


    public long getTimeout() {
        return 5000L;
    }



}

