package com.zlp.gateway.order.service.impl;

import com.zlp.gateway.order.dto.OrderDTO;
import com.zlp.gateway.order.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j(topic = "OrderServiceImpl")
public class OrderServiceImpl implements OrderService {


    @Override
    public OrderDTO getOrder() {

        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setOrderNo("202112270056789");
        orderDTO.setOrderName("Test order name");
        orderDTO.setPrice(10000L);
        orderDTO.setTitle("Test order title");
        orderDTO.setUserId(88L);
        return orderDTO;
    }
}
