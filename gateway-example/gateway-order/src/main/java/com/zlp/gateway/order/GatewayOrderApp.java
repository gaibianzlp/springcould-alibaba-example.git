package com.zlp.gateway.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GatewayOrderApp {

    public static void main(String[] args) {
        SpringApplication.run(GatewayOrderApp.class,args);
    }

}
