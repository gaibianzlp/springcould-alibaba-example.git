package com.zlp.gateway.order.dto;


import lombok.Data;

@Data
public class OrderDTO {

    private String orderNo;
    private String orderName;
    private Long price;
    private String title;
    private Long userId;
}
