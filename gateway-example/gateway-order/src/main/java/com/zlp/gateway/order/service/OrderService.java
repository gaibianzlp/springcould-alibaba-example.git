package com.zlp.gateway.order.service;

import com.zlp.gateway.order.dto.OrderDTO;

public interface OrderService {

    OrderDTO getOrder();
}
