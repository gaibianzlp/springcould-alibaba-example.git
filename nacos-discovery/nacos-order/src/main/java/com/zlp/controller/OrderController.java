package com.zlp.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class OrderController {


    @GetMapping("/getOrder")
    public String getOrder(@RequestParam String name) {

        log.info("invoked name = " + name);
        return "使用-->>"+name;
    }
}
