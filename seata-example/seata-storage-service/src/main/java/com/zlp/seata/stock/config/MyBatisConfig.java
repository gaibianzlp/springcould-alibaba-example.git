package com.zlp.seata.stock.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("com.zlp.seata.stock.mapper")
public class MyBatisConfig {

}
