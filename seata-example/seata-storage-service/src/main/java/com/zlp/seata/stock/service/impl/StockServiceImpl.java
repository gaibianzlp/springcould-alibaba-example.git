package com.zlp.seata.stock.service.impl;


import com.zlp.seata.stock.mapper.StockMapper;
import com.zlp.seata.stock.service.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/***
 * @Author 徐庶   QQ:1092002729
 * @Slogan 致敬大师，致敬未来的你
 */
@Service
public class StockServiceImpl implements StockService {

    @Resource
    StockMapper stockMapper;

    @Override
    public void reduct(Integer productId) {

        System.out.println("更新商品:"+productId);
        stockMapper.reduct(productId);
    }
}
