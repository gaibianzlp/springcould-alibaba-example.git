package com.zlp.seata.order.mapper;

import com.zlp.seata.order.entity.Order;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface OrderMapper {

    int insert(Order record);


}