package com.zlp.seata.order.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * 扫描Mapper接口，整合MyBatis
 * @date: 2021/11/30 9:42
 */
@Configuration
@MapperScan("com.zlp.seata.order.mapper")
public class MyBatisConfig {

}
