package com.zlp.ribbon.order.controller;


import com.zlp.ribbon.order.dto.Order;
import com.zlp.ribbon.order.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class OrderController {


    private final OrderService orderService;

    /**
     * 添加订单
     * @date: 2021/12/3 14:19
     */
    @GetMapping("addOrder")
    public Order addOrder(){
        return orderService.addOrder();
    }


}
