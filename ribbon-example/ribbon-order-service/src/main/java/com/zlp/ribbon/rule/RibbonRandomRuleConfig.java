package com.zlp.ribbon.rule;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RibbonRandomRuleConfig {

    /**
     * 方法名一定要叫iRule
     * @date: 2021/12/3 16:49
     * @return: com.netflix.loadbalancer.IRule
     */
    @Bean
    public IRule iRule(){
        return new RandomRule();
    }
}
