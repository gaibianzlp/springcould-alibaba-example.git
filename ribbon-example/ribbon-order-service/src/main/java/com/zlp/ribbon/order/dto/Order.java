package com.zlp.ribbon.order.dto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class Order implements Serializable {

    private Integer id;
    private String name;
    private BigDecimal amount;
}
