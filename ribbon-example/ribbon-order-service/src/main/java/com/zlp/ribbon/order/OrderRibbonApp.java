package com.zlp.ribbon.order;


import com.zlp.ribbon.rule.RibbonRandomRuleConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.netflix.ribbon.RibbonClients;

@SpringBootApplication
@RibbonClients(value = {
        @RibbonClient(name="ribbon-stock",configuration = RibbonRandomRuleConfig.class)
})
public class OrderRibbonApp {

    public static void main(String[] args) {
        SpringApplication.run(OrderRibbonApp.class,args);
    }

}
