package com.zlp.ribbon.order.service.impl;
import java.math.BigDecimal;

import com.zlp.ribbon.order.dto.Order;
import com.zlp.ribbon.order.service.OrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@RequiredArgsConstructor
@Slf4j(topic = "OrderServiceImpl")
public class OrderServiceImpl implements OrderService {


    private final RestTemplate restTemplate;

    @Override
    public Order addOrder() {
        Order order = new Order();
        order.setId(1);
        order.setName("ribbon 订单");
        order.setAmount(new BigDecimal("10098"));
//        String url = "http://127.0.0.1:8130/reduct";
        String url = "http://ribbon-stock/reduct";
        String object = restTemplate.getForObject(url, String.class);
        log.info("object={}",object);
        return order;
    }
}
