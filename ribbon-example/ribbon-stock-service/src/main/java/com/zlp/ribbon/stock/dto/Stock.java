package com.zlp.ribbon.stock.dto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class Stock implements Serializable {

    private Integer id;
    private String name;
    private BigDecimal amount;
}
