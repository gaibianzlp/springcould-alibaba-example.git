package com.zlp.ribbon.stock.controller;


import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class StockController {


    @Value("${server.port}")
    private String port;

    /**
     * 扣减库存
     *
     * @date: 2021/12/3 14:19
     */
    @GetMapping("reduct")
    public String reduct() {

        return "扣减库存success" + port;
    }


}
