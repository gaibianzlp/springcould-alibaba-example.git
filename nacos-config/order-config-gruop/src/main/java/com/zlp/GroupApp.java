package com.zlp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @description: //TODO
 * @author: LiPing.Zou
 * @create: 2020-05-18 17:02
 **/
@SpringBootApplication
public class GroupApp {

    public static void main(String[] args) {
        SpringApplication.run(GroupApp.class,args);
    }
}
